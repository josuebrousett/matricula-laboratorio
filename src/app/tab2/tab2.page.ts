import { Component } from '@angular/core';
import { disableDebugTools } from '@angular/platform-browser';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

  private cursos = [
    {
      id: '1',
      name: 'Plataformas Emergentes',
      dia: 'martes',
      horaInicio: '8',
      horaFin: '10'
    },
    {
      id: '2',
      name: 'Topicos Avanzados en Ingenieria de Software',
      dia: 'lunes',
      horaInicio: '12',
      horaFin: '13'
    },
    {
      id: '3',
      name: 'Desarrollo de Software para Juegos',
      dia: 'miercoles',
      horaInicio: '14',
      horaFin: '16'
    },
    {
      id: '4',
      name: 'Proyecto de Ingenieria de Software 2',
      dia: 'jueves',
      horaInicio: '15',
      horaFin: '17'
    }
  ]

  constructor() { }

}
